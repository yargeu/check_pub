from copy import copy

# CheckButton에서 self.had_catch 속성은 History에서 self.changed_atts가 대각선으로 두칸 이동했다면 업데이트 시켜주기(ctrl + z했을 때)
class History:
    def __init__(self, grid, turn, ckd, before_history = None):  # None은 정확히 어느 상황에 두는 거에요?
        self.grid = grid
        self.grid[2] = list(set([(i, j) for i in range(10) for j in range(10)]) - set(self.grid[0] +self.grid[1]))
        self.turn = turn
        self.ckd = ckd
        self.before_history = before_history
        self.compare_atts()
        
    def compare_atts(self):    # history 하나당 자신의 속성으로 무엇이 어떻게 변했는지 가지고 있는 것이다.(내 생각)
        if self.before_history is None:
            self.changed_atts = None
            return
        self.changed_atts = {}
        self.changed_atts['changed_pt'] = {0:[tuple(),tuple()], 1:[tuple(),tuple()]}
        for i in range(2):
            for bef_pt in self.before_history.grid[i]:
                if bef_pt not in self.grid[i]: # 지워진 좌표
                    self.changed_atts['changed_pt'][i][0] = bef_pt
                    self.past_bt = bef_pt
                    break
            for po_pt in self.grid[i]:
                if po_pt not in self.before_history.grid[i]:
                    self.changed_atts['changed_pt'][i][1] = po_pt
                    self.now_bt = po_pt
                    break
        
        self.changed_atts['turn'] = (self.before_history.turn, self.turn)
        self.changed_atts['changed_ckd'] = (self.before_history.ckd, self.ckd)   # 연속해서 말을 잡을 경우 변화가 있을 것이다.
        # 그리드 변경점 잘 돌아가는지 확인
        # 나머지 속성들도 변경이력 추가
        return